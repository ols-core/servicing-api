﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace servicing_api.Interfaces
{
    public interface IOffersOrchestrator
    {
        string MovingOffer();
    }
}
