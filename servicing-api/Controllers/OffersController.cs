﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using servicing_api.Interfaces;

namespace servicing_api.Controllers
{
    public class OffersController : BaseController
    {
        private readonly IOffersOrchestrator _orchestrator;

        public OffersController(IOffersOrchestrator offersOrchestrator)
        {
            _orchestrator = offersOrchestrator;
        }

        [HttpGet]
        public string Get()
        {
            return _orchestrator.MovingOffer();
        }

    }
}
