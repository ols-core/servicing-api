﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using servicing_api.Interfaces;
using Microsoft.Extensions.Options;
using System.Diagnostics;
using servicing_api.Models;

namespace servicing_api.Services
{
    public class OffersOrchestrator : IOffersOrchestrator
    {
        private readonly IOptions<URLs> _urls;

        public OffersOrchestrator(IOptions<URLs> urls)
        {
            _urls = urls;
        }

        public string MovingOffer()
        {
            var stopwatch = Stopwatch.StartNew();
            return $"{_urls.Value.PartnerExpIdUrl}";
        }
    }
}
